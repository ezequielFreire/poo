/*
 * Utilizacion de JAXB para serializar xml y deserealizar
 *
 */

package serializacionJAXB;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Ezequiel A. Freire
 */
@XmlRootElement(name= "Usu")
@XmlType(propOrder = {"nombre","rol","anios","genero"})
public class Usuario {
        
    // <editor-fold defaultstate="collapsed" desc="Atributos">


 
    private int id;
    private String nombre;
    private String rol;
    private int anios;
    private String genero;
    private String password;
 
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Getter y Setter">
 
    @XmlTransient
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
   
    @XmlAttribute
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public int getAnios() {
        return anios;
    }

    public void setAnios(int anios) {
        this.anios = anios;
    }

    @XmlElement(name = "gen")
    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Constructores">
  
    public Usuario(){
        
    }

    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Metodos">
    @Override
    public String toString() {
        return "Id= " + id + " Nombre= " + nombre + " Rol=" +
                rol +" Años= " + anios + " Genero= " + genero  + " Password= " + password;
    }
    

    
    // </editor-fold>
  
}
