/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package serializacionJAXB;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Ezequiel
 */
public class Program {

    private static final String FILE_NAME = "jaxb1.xml";
    
    public static void main(String[] args) {

        Usuario usu = new Usuario();
        usu.setId(1);
        usu.setAnios(28);
        usu.setNombre("Pedro");
        usu.setGenero("masculino");
        usu.setRol("Programador");
        usu.setPassword("123");
        
        
        serializarXML(usu);
        Usuario desUsu = deserializarXML();
        System.out.println(desUsu.toString());
    }
    
    public static void serializarXML(Usuario usu) {
        try {
            JAXBContext contex = JAXBContext.newInstance(Usuario.class);
            Marshaller marsh = contex.createMarshaller(); // consiste en generar del objecto un XML
            marsh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marsh.marshal(usu, new File(FILE_NAME));

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
    
    public static Usuario deserializarXML(){
        try {
            JAXBContext context = JAXBContext.newInstance(Usuario.class);
            Unmarshaller un = context.createUnmarshaller();
            Usuario usu = (Usuario) un.unmarshal(new File(FILE_NAME));
            return usu;
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }
}
