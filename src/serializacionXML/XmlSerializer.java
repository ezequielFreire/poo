/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package serializacionXML;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author Ezequiel A. Freire
 */
public class XmlSerializer {
    
    private boolean serializo;
    
    public XmlSerializer(){
        
    }
    
    public boolean guardaObjeto(Usuario unUsuario) {
         
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        FileOutputStream path = null; //= new FileOutputStream("/miObjeto.xml");
        XMLStreamWriter escribir = null;
        File dir;
        try{
            dir = new File("C:\\Temporales2");
            if(!dir.exists()){
                dir.mkdir();
            }
            path = new FileOutputStream(dir+"\\miObjeto.xml",true);
            escribir = factory.createXMLStreamWriter(path);
            
            //Abrimos tag de incio
            escribir.writeStartElement("usuario");
            
            //abrimos y cerramos tag nombre
            escribir.writeStartElement("nombre");
	    escribir.writeCharacters(unUsuario.getNombre());
	    escribir.writeEndElement();
            
            //abrimos y cerramos tag apellido
            escribir.writeStartElement("apellido");
	    escribir.writeCharacters(unUsuario.getApellido());
	    escribir.writeEndElement();
            
            //abrimos y cerramos tag nombre
            escribir.writeStartElement("cargo");
	    escribir.writeCharacters(unUsuario.getCargo());
	    escribir.writeEndElement();
            
            //abrimos y cerramos tag nivel de acceso
            escribir.writeStartElement("nivel");
	    escribir.writeCharacters(unUsuario.getNivelAcceso());
	    escribir.writeEndElement();
            
            //Cerramos tag de fin
            escribir.writeEndDocument();
            
            //Cerramos los archivos xml
            escribir.flush();
            escribir.close();
	    path.close();
            
        }catch(IOException | XMLStreamException ea){
            ea.getMessage();
        }finally{
            this.serializo = true;
            return this.serializo; 
        }
    }
}
