/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package serializacionXML;

import java.io.FileNotFoundException;

/**
 *
 * @author Ezequiel A. Freire
 */
public class Program {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        // Instancia de usuario
        Usuario usu = new Usuario("Pepito", "Perez", "encargado", "2");
        
        //Mostrar informacion por consola
        System.err.println(usu.toString());
        
        //Serializar objeto a XML
        if(usu.serializar()){
            System.out.println("El archivo se serializo sin problemas");
        }else{
            System.out.println("El archivo no se pudo serializar");
        }
        
//        Usuario usuDos = new Usuario();
//        usuDos = usu.deserializar();
    }
    
}
