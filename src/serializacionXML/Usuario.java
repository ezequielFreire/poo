/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package serializacionXML;

import java.io.FileNotFoundException;



/**
 *
 * @author Ezequiel A. Freire
 */
public class Usuario{
    
    //Atributos
    private String _nombre;
    private String _apellido;
    private String _cargo;
    private String _nivelAcceso;

    //Getter y Setter
    public String getNombre() {
        return _nombre;
    }

    public String getApellido() {
        return _apellido;
    }

    public String getCargo() {
        return _cargo;
    }

    public String getNivelAcceso() {
        return _nivelAcceso;
    }

    public void setApellido(String _apellido) {
        this._apellido = _apellido;
    }

    public void setNombre(String _nombre) {
        this._nombre = _nombre;
    }

    public void setCargo(String _cargo) {
        this._cargo = _cargo;
    }

    public void setNivelAcceso(String _nivelAcceso) {
        this._nivelAcceso = _nivelAcceso;
    }
    
    //Constructores
    public Usuario() {
    
    }

    public Usuario(String _nombre, String _apellido, String _cargo, String _nivelAcceso) {
        this._nombre = _nombre;
        this._apellido = _apellido;
        this._cargo = _cargo;
        this._nivelAcceso = _nivelAcceso;
    }

    //Metodos
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nnombre: "+this._nombre);
        sb.append("\napellido: "+this._apellido);
        sb.append("\ncargo: "+this._cargo);
        sb.append("\nnivel de acceso: "+this._nivelAcceso);
        return sb.toString();
    }
    
    public boolean serializar() throws FileNotFoundException{
        XmlSerializer serializar = new XmlSerializer();
        boolean res = false;
        res = serializar.guardaObjeto(this);
           
        return res;
    }
    
//    public Usuario deserializar(){
//        XmlDeserealizer des = new XmlDeserealizer();
//        des.convertirObjeto();
//        Usuario usu = null;
//        return usu;
//    }
    
}
