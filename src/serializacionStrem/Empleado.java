/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package serializacionStrem;

/**
 *
 * @author Ezequiel A. Freire
 */
public class Empleado extends Persona{
    
    // <editor-fold desc="Atributos">
    private String _tel;
    private String _localNombre;
    private String _problema;
    
    // </editor-fold>
    
    // <editor-fold desc="Getter y Setter">
    /**
     * @return the tel
     */
    public String getTel() {
        return _tel;
    }

    /**
     * @param tel the tel to set
     */
    public void setTel(String tel) {
        this._tel = tel;
    }

    /**
     * @return the LocalNombre
     */
    public String getLocalNombre() {
        return _localNombre;
    }

    /**
     * @param LocalNombre the LocalNombre to set
     */
    public void setLocalNombre(String LocalNombre) {
        this._localNombre = LocalNombre;
    }

    /**
     * @return the _problema
     */
    public String getProblema() {
        return _problema;
    }

    /**
     * @param _problema the _problema to set
     */
    public void setProblema(String _problema) {
        this._problema = _problema;
    }
    // </editor-fold>
    
    // <editor-fold desc="Constructores">
    public Empleado(){
        super();
    }
    public Empleado(String nom, String ape, String mail, String tel, String local, String problema){
        this.setNombre(nom);
        this.setApellido(ape);
        this.setMail(mail);
        this._tel = tel;
        this._localNombre = local;
        this._problema = problema;
    }    
    // </editor-fold>
    
    // <editor-fold desc="Metodos">

    // </editor-fold>

    
}
