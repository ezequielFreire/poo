/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package serializacionStrem;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

/**
 *
 * @author Ezequiel A. Freire
 */
public class Guardar {
    
    BufferedWriter escritor;
    
    public Guardar(){
        
    }
    
    public void guardarEmpleado(Empleado empleado, String ruta){
        Date fecha = new Date();
        try{
                     
            BufferedWriter escritor = new BufferedWriter(new FileWriter(ruta,true)); 
            
            escritor.newLine();
            escritor.write("****************** "+ fecha + " *********************");
            escritor.newLine();
            escritor.write("Nombre: " + empleado.getNombre());
            escritor.newLine();
            escritor.write("Apellido: " + empleado.getApellido());
            escritor.newLine();
            escritor.write("Local: " + empleado.getLocalNombre());
            escritor.newLine();
            escritor.write("Mail: " + empleado.getMail());
            escritor.newLine();
            escritor.write("Telefono: " + empleado.getTel());
            escritor.newLine();
            escritor.write("Problema: " + empleado.getProblema());
            escritor.newLine();
            escritor.write("***************************************************************************");
            escritor.newLine();
            escritor.close();

            
        }catch(IOException ex){
            try{
                BufferedWriter es = new BufferedWriter(new FileWriter("c:\\errores.txt",true));
             
                es.write("******************************");
                es.write("El dia: "+ new Date());
                es.write("Se produjo el siguiente error: " + ex.getMessage());
                es.write("******************************");
              
            }catch(IOException e){
                
            }
        }
        
        
        
    }
    
}
