/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package serializacionStrem;

/**
 *
 * @author Ezequiel A. Freire
 */
public class Persona {
    
    // <editor-fold desc="Atributos">
    
    private String _nombre;
    private String _apellido;
    private String _mail;
    
    // </editor-fold>
    
    // <editor-fold desc="Getter y Setter">
    /**
     * @return the _nombre
     */
    public String getNombre() {
        return _nombre;
    }

    /**
     * @param _nombre the _nombre to set
     */
    public void setNombre(String _nombre) {
        this._nombre = _nombre;
    }

    /**
     * @return the _apellido
     */
    public String getApellido() {
        return _apellido;
    }

    /**
     * @param _apellido the _apellido to set
     */
    public void setApellido(String _apellido) {
        this._apellido = _apellido;
    }

    /**
     * @return the _mail
     */
    public String getMail() {
        return _mail;
    }

    /**
     * @param _mail the _mail to set
     */
    public void setMail(String _mail) {
        this._mail = _mail;
    }
    // </editor-fold>
    
    // <editor-fold desc="Constructores">
    public Persona(){
        
    }
    
    public Persona(String nombre, String apellido, String mail){
        this._nombre = nombre;
        this._apellido = apellido;
        this._mail = mail;
    }
    // </editor-fold>
    
    // <editor-fold desc="Metodos">

    // </editor-fold>

    
}
