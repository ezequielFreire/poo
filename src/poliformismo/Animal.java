/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package poliformismo;

/**
 *
 * @author Ezequiel
 */
public abstract class Animal {
        
    // <editor-fold defaultstate="collapsed" desc="Atributos">
    protected int _patas;
    
    // <editor-fold defaultstate="collapsed" desc="Constructores">
    public Animal() {
    
    }
    
    
    public Animal(int _patas) {
        this._patas = _patas;
    }
    // </editor-fold>
   
    // <editor-fold defaultstate="collapsed" desc="Metodos">
    // </editor-fold>
 

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Metodos">
    public abstract void caminar();
    
    public abstract void comer();
    // </editor-fold>



    
    




}
