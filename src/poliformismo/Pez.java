/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package poliformismo;

/**
 *
 * @author Ezequiel
 */
public class Pez extends Animal implements Mascota{
    
    // <editor-fold defaultstate="collapsed" desc="Atributos">
    private String _nombre;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Getter y Setter">
    @Override
    public String getNombre() {
        return _nombre;
    }

    @Override
    public void setNombre(String _nombre) {
        this._nombre = _nombre;
    }


    
    // <editor-fold defaultstate="collapsed" desc="Metodos">
    // </editor-fold>


    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Constructores">
    public Pez() {
    }

    public Pez(String nombre, int _patas) {
        super(_patas);
        this._nombre = nombre;
    }
    // </editor-fold>
 
    // <editor-fold defaultstate="collapsed" desc="Metodos">
    @Override
    public void caminar() {
        System.out.println("Este animal no tiene patas");
    }

    @Override
    public void comer() {
        System.out.println("Este animal come algas");
    }

    @Override
    public void jugar() {
       System.out.println("Este animal juega con burbujas");  
    }
    // </editor-fold>
   
}
