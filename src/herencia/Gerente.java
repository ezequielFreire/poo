/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author Ezequiel A. Freire
 */
public class Gerente extends Persona{
        
    // <editor-fold defaultstate="collapsed" desc="Atributos">
    private double _sueldo;
    private double _presupuesto;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Getter y Setter">
    /**
     * @return the _sueldo
     */
    public double getSueldo() {
        return _sueldo;
    }

    /**
     * @param _sueldo the _sueldo to set
     */
    public void setSueldo(double _sueldo) {
        this._sueldo = _sueldo;
    }

    /**
     * @return the _presupuesto
     */
    public double getPresupuesto() {
        return _presupuesto;
    }

    /**
     * @param _presupuesto the _presupuesto to set
     */
    public void setPresupuesto(double _presupuesto) {
        this._presupuesto = _presupuesto;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Constructores">
    public Gerente(){
        super();
    }
    
    public Gerente(String nombre, int peso, int altura, String sexo, double sueldo, double presupuesto){
        super(nombre, peso, altura, sexo);
        this._sueldo = sueldo;
        this._presupuesto = presupuesto;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Metodos">
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append(super.toString());
        res.append("\nSueldo: " + this._sueldo);
        res.append("\nPresupuesto: " + this._presupuesto);
        return res.toString();
    }
    // </editor-fold>



 
}
