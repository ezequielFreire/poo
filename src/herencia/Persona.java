/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author Ezequiel A. Freire
 */
public class Persona {
        
    // <editor-fold defaultstate="collapsed" desc="Atributos">
    private String _nombre;
    private int _peso;
    private int _altura;
    private String _sexo;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Getter y Setter">
     /**
     * @return the _nombre
     */
    public String getNombre() {
        return _nombre;
    }

    /**
     * @param _nombre the _nombre to set
     */
    public void setNombre(String _nombre) {
        this._nombre = _nombre;
    }

    /**
     * @return the _peso
     */
    public int getPeso() {
        return _peso;
    }

    /**
     * @param _peso the _peso to set
     */
    public void setPeso(int _peso) {
        this._peso = _peso;
    }

    /**
     * @return the _altura
     */
    public int getAltura() {
        return _altura;
    }

    /**
     * @param _altura the _altura to set
     */
    public void setAltura(int _altura) {
        this._altura = _altura;
    }

    /**
     * @return the _sexo
     */
    public String getSexo() {
        return _sexo;
    }

    /**
     * @param _sexo the _sexo to set
     */
    public void setSexo(String _sexo) {
        this._sexo = _sexo;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Constructores">
    public Persona(){
        
    }
    
    public Persona(String nombre, int peso, int altura, String sexo){
        this._nombre = nombre;
        this._peso = peso;
        this._altura = altura;
        this._sexo = sexo;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Metodos">
    public String comer(){
        String res = "Se encuentra almorzando";
        return res;
    }
    
    public boolean dormir(){
        return false;
    }
    
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("\nNombre: " + this._nombre);
        res.append("\nPeso: " + this._peso);
        res.append("\nAltura: " + this._altura);
        res.append("\nSexo: " + this._sexo);
        return res.toString();
    }

    
    // </editor-fold>


}
