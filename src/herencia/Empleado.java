/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author Ezequiel A. Freire
 */
public class Empleado extends Persona{
        
    // <editor-fold defaultstate="collapsed" desc="Atributos">
    private double _sueldo;
    private boolean _trabajando;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Getter y Setter">
    /**
     * @return the sueldo
     */
    public double getSueldo() {
        return _sueldo;
    }

    /**
     * @param sueldo the sueldo to set
     */
    public void setSueldo(double sueldo) {
        this._sueldo = sueldo;
    }

    /**
     * @return the trabajando
     */
    public boolean isTrabajando() {
        return _trabajando;
    }

    /**
     * @param trabajando the trabajando to set
     */
    public void setTrabajando(boolean trabajando) {
        this._trabajando = trabajando;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Constructores">
    public Empleado(){
        super();
    }
    
    public Empleado(String nombre, int peso, int altura, String sexo, double sueldo){
        super(nombre, peso, altura, sexo);
        this._sueldo = sueldo;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Metodos">
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append(super.toString());
        res.append("\nSueldo: " + this._sueldo);
        return res.toString();
    }
    // </editor-fold>


}
