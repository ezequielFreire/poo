package herencia;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ezequiel A. Freire
 */
public class Program {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Instancia de gerente y utilizacion de sus metodos
        Gerente unGerente = new Gerente("Pedro",75,2,"M",25000d,50500d);
        if(unGerente.dormir()){
            System.out.println("Esta dormido");
        }else{
            System.out.println("No esta dormido");
        }
        System.out.println(unGerente.toString());
        System.out.println(unGerente.comer());
 
        //Intancia de empleado
        Empleado unEmpleado = new Empleado("Juan", 80, 2, "M", 7000d);
        if(unEmpleado.dormir()){
            System.out.println("Esta dormido");
        }else{
            System.out.println("No esta dormido");
        }
        System.out.println(unEmpleado.toString());
        System.out.println(unEmpleado.comer());
    }
    
}
